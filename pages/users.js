
import UserList from '../components/Users/UserList.js'
import Header from '../components/Header/Header.js'
import Error from '../components/Error/Error.js'
import Loading from '../components/Loading/Loading.js'

import renderHtml from '../utils/renderHtml.js'
import queryString from '../utils/queryString.js';
import setActivePage from '../utils/setActivePage.js'

import config from '../config.js';

// set the render element to the page and assign the event listener to
// the button(next, previous, search) to trigger actions
// finally update the current navigation
const initPage = () => {
  renderHtml('root', render);

  const query = queryString.getQuerySearch() || {};
  
  const nextElement = document.querySelectorAll('.user-next');

  if(query) loadUser();

  nextElement.forEach(item => {
    item.addEventListener('click', handleNextPage);
  })
  
  
  const prevElement = document.querySelectorAll('.user-prev');
  prevElement.forEach(item => {
    item.addEventListener('click', handlePrevPage);
  });

  const searchElement = document.getElementById('search');
  searchElement.value = query.q ? query.q : '';
  searchElement.addEventListener('keyup', handleSearch);

  setActivePage('users');
}

/*
* Api call loading user data
*/
const loadUser = () => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};

  if(!q) return;

  renderHtml('user',Loading());

  fetch(`${config.baseUrl}/search/users?since=${page}&per_page=10&q=${q}`)
  .then(res => {
    if (res.ok) {
      return res.json();
    }
  
    throw Error(`Unable to fetch github users [${res.status}]`);
  })
  .then(data => renderHtml('user', UserList(data.items)))
  .catch(err => renderHtml("user", Error(err)))

}

// hash will change and then Router.js start again and then pageInit start
// finally api called with new pages
const handlePrevPage =() => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};

  if(page && q) {
    const currentPage = parseInt(page);
    
    if(currentPage < 1) {
      location.hash = '#users?page=1'
    }
    else {
      let calculateCurrentPage = currentPage - 10;
      if(calculateCurrentPage < 1) calculateCurrentPage = 1;
      location.hash = `#users?page=${calculateCurrentPage}&q=${q}`
    }
  }
}


// hash will change and then Router.js start again and then pageInit start
// finally api called with new pages

const handleNextPage = () => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};
  
  if(page && q) {
    const currentPage = parseInt(page);

    if(currentPage < 1) {
      location.hash = '#users?page=1'
    }
    else {
      let calculateCurrentPage = currentPage + (currentPage=== 1 ? 9 : 10);

      if(calculateCurrentPage < 1) calculateCurrentPage = 1;

      location.hash = `#users?page=${calculateCurrentPage}&q=${q}`
    }
  }
}

//check if user press Enter the change the hash to trigger api with search value
const handleSearch = (event) => {
  if(event.keyCode === 13) {
    const searchElement = document.getElementById('search');
    const q = searchElement.value;

    if(!q) return;
    
    location.hash = `#users?page=1&q=${q}`
  }
  
 
}


const render = 
  `
  <div class="container">
    ${Header()}
    
    <div class="container--wrapper">
      <div class="page-title">
        <h1>User Page</h1>
      </div>
      <div class="box-list--header">
        <div class="search-box">
          <input type="text" id="search" placeholder="Search for users" name="search">
        </div>
        <div class="top-pagination">
          <div>
            <button class="user-prev btn" type="button">Prev</button>
            <button class="user-next btn" type="button">Next</button>
          </div>
        </div>
      </div>
      <div id="user" class="user-content">
        <p class="default-message">Please Search a Query</p>
      </div>
      <div class="bottom-pagination">
        <div>
          <button class="user-prev btn" type="button">Prev</button>
          <button class="user-next btn" type="button">Next</button>
        </div> 
      </div>
    </div>
  </div>`




export default { initPage };
