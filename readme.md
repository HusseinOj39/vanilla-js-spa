# Vanilla JS SPA

The Project implement the Single Page application. it's use the browser hash capability to implement the routing.


## File Direcotries

/assets: hold the application Images
/Components:
/Pages:
/theme: style are placed here and orgnaized base on the requirement
/utils: 
  Router.js: handle the routing of the application by detecting the current route in location.hash
 
## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## License
[MIT](https://choosealicense.com/licenses/mit/)

## Authors
Hussein Ojaghi
