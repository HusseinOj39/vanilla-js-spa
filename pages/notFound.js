
import renderHtml from '../utils/renderHtml.js'

import Header from '../components/Header/Header.js'
const initPage = () => {
  renderHtml('root', render);
}


const render =`
  <div class="container">
    ${Header()}
    <div class="not-found--wrapper">
      <h1>
        <img src="../assets/notFound.png" alt="not found"/>
        <span>Page Not Found</span>
      </h1>
      <p>
        The Page is not exists.
      </p>
    </div>
  </div>
`;


export default { initPage };