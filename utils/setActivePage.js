// use this method to select the current page navigation
const setActivePage = (activePage) => {
    const element = document.getElementById('user-navigation')
                            .getElementsByTagName("li");
   
    // bind this to forEach and remove all active
    [].forEach.call(element, function(el) {
        el.classList.remove("active");
    });

    // add active class to current page
    [].forEach.call(element, function(el) {
        if(el.getAttribute('data_id') === activePage)
          el.classList.add("active");
    });
}

export default setActivePage;