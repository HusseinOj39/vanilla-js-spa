// I use this component both in users page and userDetails page
// path isUserDetail to detect the differences.
const UserItem = (user, isUserDetail) => {
    const avatar = user.avatar_url;
    const userName = user.login;

    const renderUserInfo = () => `
      <div class="avatar">
         <img src="${avatar}" alt="${userName}" />
      </div>
      <div class="user-name">
         ${userName}
      </div>`;

    return `
        <div>
          ${!isUserDetail ? 
            `<a class="user-item hover-item" href="#user/${userName}">
              ${renderUserInfo()}
             </a>`:
            `<div class="user-item">${renderUserInfo()}</div>`} 
        </div>`
}


export default UserItem;
