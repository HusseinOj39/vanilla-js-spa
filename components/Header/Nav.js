import { getPageName } from '../../utils/queryString.js';
const Nav = () => {
  const activeMenu = getPageName();

    return `
        <ul id="user-navigation" class="nav">
          <li data_id="users" class="${activeMenu === 'users'? 'active':''}">
            <a href="#users">Users</a>
          </li>
          <li data_id="repository" class="${activeMenu === 'repository'? 'active':''}">
            <a href="#repository">Repositories</a>
          </li>
        </ul>`;
};

export default Nav;