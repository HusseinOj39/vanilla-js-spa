import Nav from './Nav.js'

const Header = () => {
    return `
          <div class="header-nav">
            <div class="logo">
              <img src="../../assets/github.png" alt="logo" />
            </div>
            <div>
            ${Nav()}
            </div>
          </div>
        `
}

export default Header;