import RepositoryItem from './RepositoryItem.js';

const RepositoryList = (data) => {
    const renderRepositoryList = data.map(item => RepositoryItem(item));
   
    return `<div class="repo-list">
              ${renderRepositoryList.join('')} 
            </div>`
}

export default RepositoryList;