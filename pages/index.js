import NotFound from './notFound.js';
import Repository from './repository.js';
import Users from './users.js';
import UserDetails from './userDetails.js';

export default {
    NotFound,
    Repository,
    Users,
    UserDetails
}