
import RepositoryList from '../components/Repository/RepositoryList.js'
import Header from '../components/Header/Header.js'
import Loading from '../components/Loading/Loading.js'
import Error from '../components/Error/Error.js'
import renderHtml from '../utils/renderHtml.js'
import queryString from '../utils/queryString.js';
import setActivePage from '../utils/setActivePage.js'
import config from '../config.js';

// set the render element to the page and assign the event listener to
// the button(next, previous, search) to trigger actions
// finally update the current navigation
const initPage = () => {
  renderHtml('root', render);

  const query= queryString.getQuerySearch() || {};
  const nextElement = document.querySelectorAll('.repository-next');

  if(query) loadRepository();

  nextElement.forEach(item => {
    item.addEventListener('click', handleNextPage);
  })
  
  
  const prevElement = document.querySelectorAll('.repository-prev');
  prevElement.forEach(item => {
    item.addEventListener('click', handlePrevPage);
  })

  const searchElement = document.getElementById('search');
  searchElement.value = query.q ? query.q : '';
  searchElement.addEventListener('keyup', handleSearch);

  setActivePage('repository')
}

const loadRepository = () => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};

  if(!q) return;

  renderHtml('repository',Loading());

  fetch(`${config.baseUrl}/search/repositories?since=${page}&per_page=10&q=${q}`)
  .then(res => {
    if (res.ok) {
      return res.json();
    }
  
    throw Error(`Unable to fetch github Repository [${res.status}]`);
  })
  .then(data => {
    renderHtml('repository', RepositoryList(data.items))
  })
  .catch(err => renderHtml("repository", Error(err)))

}

// hash will change and then Router.js start again and then pageInit start
// finally api called with new pages
const handlePrevPage =() => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};

  if(page && q) {
    const currentPage = parseInt(page);
    
    if(currentPage < 1) {
      location.hash = '#repository?page=1'
    }
    else {
      let calculateCurrentPage = currentPage - 10;

      if(calculateCurrentPage < 1) calculateCurrentPage = 1;

      location.hash = `#repository?page=${calculateCurrentPage}&q=${q}`
    }
  }
}

const handleNextPage = () => {
  const { page = 1, q = '' } = queryString.getQuerySearch() || {};
  
  if(page && q) {
    const currentPage = parseInt(page);
    if(currentPage < 1) {
      location.hash = '#repository?page=1'
    }
    else {
      let calculateCurrentPage = currentPage + (currentPage=== 1 ? 9 : 10);

      if(calculateCurrentPage < 1) calculateCurrentPage = 1;
      
      location.hash = `#repository?page=${calculateCurrentPage}&q=${q}`
    }
  }
}

//check if user press Enter the change the hash to trigger api with search value
const handleSearch = (event) => {
  if(event.keyCode === 13) {
    const searchElement = document.getElementById('search');
    const q = searchElement.value;
    location.hash = `#repository?page=1&q=${q}`
  }
  
 
}
const render = (
  `
  <div class="container">
    ${Header()}
    <div class="container--wrapper">
      <div class="page-title">
          <h1>Repository Page</h1>
      </div>

      <div class="box-list--header">
        <div class="search-box">
          <input type="text" id="search" placeholder="Search for Repository" name="search">
        </div>
        <div class="top-pagination">
          <div>
            <button class="repository-prev btn" type="button">Prev</button>
            <button class="repository-next btn" type="button">Next</button>
          </div>
        </div>
      </div>
      <div id="repository"  class="user-content">
        <p class="default-message">Please Enter a Query</p>
      </div>
      <div class="bottom-pagination">
        <div>
          <button class="repository-prev btn" type="button">Prev</button>
          <button class="repository-next btn" type="button">Next</button>
        </div> 
      </div>
    </div>
  </div>`);



export default { initPage };
