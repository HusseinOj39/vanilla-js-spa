import UserItem from './UserItem.js';
import UserDetail from './userDetail.js'
// gather user item and userDetails together for more control 
const userDetailBox = (data) => {
    return `<div class="user-details-box--wrapper">
              ${UserItem(data, true)}
              ${UserDetail(data)}
            </div>`
}

export default userDetailBox;