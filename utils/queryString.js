
// check is there any page in route 
// then return the the position of the querystring
const isPageExist = (hash) => {
   const hasPage = hash.indexOf('?');

   if(hasPage === -1) {
       return hash.length;
   } else {
       return hash.indexOf('?') - 1
   }
}

// looking for the querystring(page and search)
// return an object to access to it
export function getQuerySearch() {
    const hash = window.location.hash;

    if(hash.indexOf('?') === -1) return;

    const query = hash.substr(hash.indexOf('?') + 1, hash.length);
    
    const queryKeys = query.split('&');

    const value = {};

    queryKeys.forEach(item => {
        const splitItem = item.split('=');
        value[splitItem[0]] = splitItem[1];
    });

    return value;
}


export function getPageName() {
    const hash = window.location.hash;
    return hash.substr(1, isPageExist(hash, false))
}

export default {
    getQuerySearch,
    getPageName
}