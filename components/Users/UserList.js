import UserItem from './UserItem.js';

const UserList = (data) => {
    const renderUserList = data.map(item => UserItem(item));
    return `<div class="user-list">
              ${renderUserList.join('')} 
            </div>`
}

export default UserList;