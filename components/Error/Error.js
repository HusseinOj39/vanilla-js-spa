const Error = (err) => {
    return `
          <div class="error--message">
            <span> ${err}</span>
          </div>
        `
}

export default Error;