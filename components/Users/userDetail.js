
// only use in user details page as the user info part
const userDetail = (user) => {
    const type = user.type;
    const twitterUserName = user.twitter_username;
    const bio = user.bio
    const company = user.company;
    const email = user.email;
    return `
        <div class="user-detail--box">
            <div class="top-box">
                <div class="left">
                    <p><strong>Type:</strong><span>${type}</span></p>
                    <p><strong>Twitter:</strong><span>${twitterUserName}</span></p>
                </div>
                <div class="right">
                    <p><strong>Company:</strong><span>${company}</span></p>
                    <p><strong>Email:</strong><span>${email}</span></p>
                </div>
            </div>
            <div class="bio">
                <p><strong>Bio:</strong>${bio}</p>
            </div>
        </div>
    `
 
}

export default userDetail;