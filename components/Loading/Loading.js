const Loading = () => {
  return `
        <div class="loading--container">
          <img src="../assets/loading.gif" alt="loading" />
        </div>
      `
}

export default Loading;