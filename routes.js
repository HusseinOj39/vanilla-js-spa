import pages from './pages/index.js';

// all the possible route in the app and their component or page
function routes() {
    return  [
    { 
      path:'users', 
      component: pages.Users 
    },
    { 
        path: 'user', 
        component: pages.UserDetails 
    },
    { 
        path: 'repository',
        component: pages.Repository
    }
    ]
}

export default routes;