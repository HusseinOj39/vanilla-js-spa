import routes from '../routes.js';
import pages from '../pages/index.js';
import queryString from './queryString.js';
/*
* Main part of app by lookup in browser hash and detect the route and 
* load corresponding component and call initPage to initialize the page
*/
function Router() {
  const pathResolver = (path) => path.split('/')[0];  
  function displayCurrentPage() {
    const currentPage = queryString.getPageName()
    
    let page = routes().find(item => pathResolver(item.path) === pathResolver(currentPage));
    
    //when the currentPage is empty it's mean it is the first time 
    // and default page is users
    if(currentPage==='') 
      page = routes().find(item => pathResolver(item.path)==='users')
      
    const renderElement = page ? page.component : pages.NotFound;
    
    renderElement.initPage();
    return true;
  }
  
  
  window.addEventListener("hashchange", function() {
    displayCurrentPage();
  });

  window.addEventListener("DOMContentLoaded", function(ev) {
    displayCurrentPage();
  });
 
}

export default Router;