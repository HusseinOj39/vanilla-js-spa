// use this method to update the dom
const renderHtml = (id, htmlString) => {
    const element = document.getElementById(id);
    element.innerHTML = htmlString;
}

export default renderHtml;