
import RepositoryList from '../components/Repository/RepositoryList.js'
import UserDetailBox from '../components/Users/userDetailBox.js'
import Header from '../components/Header/Header.js'
import Loading from '../components/Loading/Loading.js'
import Error from '../components/Error/Error.js'

import renderHtml from '../utils/renderHtml.js'
import queryString from '../utils/queryString.js';
import setActivePage from '../utils/setActivePage.js'
import config from '../config.js';

// set the render element to the page and assign the event listener to
// the button(next, previous) to trigger actions
// finally update the current navigation
const initPage = () => {
  renderHtml('root', render);
  // load both user detail api and load repository
  loadUserDetails();
  loadRepository();
  
  const nextElement = document.querySelectorAll('.user-repository-next');
  nextElement.forEach(item => {
    item.addEventListener('click', handleNextPage);
  })
  
  const prevElement = document.querySelectorAll('.user-repository-prev');
  prevElement.forEach(item => {
    item.addEventListener('click', handlePrevPage);
  })

  const btnBack = document.getElementById('btnBack');
  btnBack.addEventListener('click', handleBackButton);

  setActivePage('users')
}

// call repository api and set the result if successful
const loadRepository = () => {
  const { page = 1 } = queryString.getQuerySearch() || {};

  renderHtml('repository',Loading());

  fetch(`${config.baseUrl}/search/repositories?since=${page}&per_page=10&q=react`)
  .then(res => {
    if (res.ok) {
      return res.json();
    }
  
    throw Error(`Unable to fetch github Repository [${res.status}]`);
  })
  .then(data => {
    renderHtml('repository', RepositoryList(data.items))
  })
  .catch(err => renderHtml("repository", Error(err)))

}

// call user details api and set the result to dom if successful
const loadUserDetails = () => {

  const currentPage = queryString.getPageName()
  const userName = currentPage.split('/')[1];
  renderHtml('user-details',Loading());

  fetch(`${config.baseUrl}/users/${userName}`)
  .then(res => {
    if (res.ok) {
      return res.json();
    }
  
    throw Error(`Unable to fetch github User Details [${res.status}]`);
  })
  .then(data => {
    renderHtml('user-details', UserDetailBox(data))
  })
  .catch(err => renderHtml("user-details", Error(err)))

}

const handlePrevPage =() => {
  const { page = 1 } = queryString.getQuerySearch() || {};
  
  if(page) {
    const currentPage = parseInt(page);
    
    if(currentPage < 1) {
      location.hash = '#repository?page=1'
    }
    else {
      let calculateCurrentPage = currentPage - 10;
      const routeName = queryString.getPageName()
      if(calculateCurrentPage < 1) calculateCurrentPage = 1;
      location.hash = `#${routeName}?page=${calculateCurrentPage}`
    }
  }
}

const handleNextPage = () => {
  const { page = 1 } = queryString.getQuerySearch() || {};
  
  if(page) {
    const currentPage = parseInt(page);
    if(currentPage < 1) {
      location.hash = '#repository?page=1'
    }
    else {
      let calculateCurrentPage = currentPage + (currentPage=== 1 ? 9 : 10);
      const routeName = queryString.getPageName()
      if(calculateCurrentPage < 1) calculateCurrentPage = 1;
      location.hash = `#${routeName}?page=${calculateCurrentPage}`
    }
  }
}
const handleBackButton = ()=> history.back();

const render = (
  `
  <div class="container">
      ${Header()}
    <div class="user-detail-wrapper">
      <div class="page-title">
        <h1>User Details</h1>
      </div>
      <div class="sample">
        <div class="back-button">
          <button id="btnBack" class="btn" type="button">Back</button>
        </div>
        <div id="user-details"></div>
        <div class="repository-title">
          <p>
            <strong>
              User Repositories
            </strong>
          </p>
          <div class="top-pagination">
            <div>
              <button class="user-repository-prev btn" type="button">Prev</button>
              <button class="user-repository-next btn" type="button">Next</button>
            </div>
          </div>
        </div>
        <div>
          <div id="repository" class="repository-item-box">
            <p class="default-message">User Repository</p>
          </div>
        </div>
        <div class="bottom-pagination margin-t-10">
          <div>
            <button class="user-repository-prev btn" type="button">Prev</button> 
            <button class="user-repository-next btn" type="button">Next</button>
          </div>
        </div>
      </div>
    </div>
  </div>`);


export default { initPage };
