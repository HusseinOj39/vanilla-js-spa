const RepositoryItem = (repository) => {
    const avatar = repository.avatar_url || 'https://avatars.githubusercontent.com/u/2?v=4';
    const repoName = repository.full_name;
    const ownerName = repository.owner.login;
    const repoType = repository.owner.type;
    const repoUrl = repository.owner.repos_url;
    return `
        <div class="repo-item">
          <div class="avatar">
            <img src="${avatar}" alt="${repoName}"/>
          </div>
          <div class="info">
            <p><strong>Repository Name:</strong>${repoName}</p>
            <p><strong>Owner Name:</strong>${ownerName}</p>
            <p><strong>Repository Type:</strong>${repoType}</p>
          </div>
          <div class="external-link">
             <a href=${repoUrl}>Check Repo</a>
           </div>
        </div>`
}

export default RepositoryItem;